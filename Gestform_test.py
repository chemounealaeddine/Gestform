import random
import time

# The maximum & minimum values of our integers
MAX_INTEGER_VALUE = 1000
MIN_INTEGER_VALUE = -1000

# The first and the second divisor
FIRST_DIV_NUM = 3
SECOND_DIV_NUM = 5

# The minimum length of our list
MIN_LIST_LENGTH = 1

if __name__ == "__main__":
    # The interval of the list of integers
    integer_list = [item for item in range(
        MIN_INTEGER_VALUE, MAX_INTEGER_VALUE)]

    # Input the length of the list of integers (It should be between 1 and the integer_list length)
    list_length = 10

    # Creation of the random list
    list_values = random.sample(integer_list, list_length)

    # Sorting the created list
    list_values.sort()

    # Display of the created list
    print("The created list is : {}".format(list_values))

    # Browse the list
    for var in list_values:
        if ((var % FIRST_DIV_NUM == 0) and (var % SECOND_DIV_NUM == 0)):  # Write Gestform
            print("{} : Gestform".format(var))
        elif (var % FIRST_DIV_NUM == 0):  # Write Geste
            print("{} : Geste".format(var))
        elif (var % SECOND_DIV_NUM == 0):  # Write Forme
            print("{} : Forme".format(var))
        else:
            print(var)

    time.sleep(5)

# Modification test 3254hdzgejjrers24164sdfrzgdfglsdnjskkjnj
# Modification test 3254hdzgejs24164sdfrzgdfglsdnjskkjnjnjnjn
# Modification test 3255
